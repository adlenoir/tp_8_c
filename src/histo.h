#ifndef HISTO_H
#define HISTO_H

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define HISTOSIZE 21

typedef int histogram_t[HISTOSIZE];

typedef struct gdata_s {
    Window         root;       
    Window         win;           
    Display       *dpy;    
    int            ecran;        
    GC             gcontext;         
    XGCValues      gcv;              
    Visual        *visual;
    Colormap       colormap;
    Font           font;
} gdata_t;

void displayHisto(histogram_t h);
int maxHisto(histogram_t h);
float meanHisto(histogram_t h);
int countHisto(histogram_t h);
void displayGraph(histogram_t h);
void displayText(histogram_t h);

#endif