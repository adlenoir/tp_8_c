#ifndef COMMUN_H
#define COMMUN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "histo.h"
#include "liste.h"

#define ERROR_OK          0
#define ERROR_LIST_ALLOC  1
#define ERROR_FILE        1

void computeHisto(histogram_t h, list_t l);
void displayGraphicalHisto(gdata_t g, histogram_t h);

#endif